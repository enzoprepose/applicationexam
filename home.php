<?php
session_start();

if (isset($_SESSION['id']) && isset($_SESSION['username'])) {
}else{
    header("Location: index.php");
    exit(); 
}
include "db_conn.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Registration Form - MiniBlog</title>
	<link rel="stylesheet" type="text/css" href="style3.css">
</head>
<body>
	<header>
		<h1>MiniBlog</h1>
        <h1>Hi, <?php echo $_SESSION['username']; ?>!</h1>
        <a href="home.php">Home</a>
		<a href="logout.php">Log out</a>
	</header>
    <?php
if (isset($_GET['error'])) {
    if ($_GET['error'] == 'error') {
        echo "<div class='error'><p>Passwords do not match</p><span class='close-btn'>&times;</span></div>";
    } else if ($_GET['error'] == 'email_exists') {
        echo "<div class='error'><p>Email address already exists</p><span class='close-btn'>&times;</span></div>";
    }
} else if (isset($_GET['success'])) {
    if ($_GET['success'] == 'true') {
        echo "<div class='success'><p>Success</p><span class='close-btn'>&times;</span></div>";
    }
}
?>
 <?php   
    $sql = "SELECT * FROM post";
    $result = mysqli_query($conn, $sql);

// Step 4: Display the data in an HTML container
echo "<div class='form-container'>";
while ($row = mysqli_fetch_assoc($result)) {
    echo "<p>" . $row['post_title'] . "</p>";
    echo "<p>" . $row['post_content'] . "</p>";
    echo "<p>" . $row['date_created'] . "</p>";
    echo "<button style='background-color: #df4759; margin: 10px' onclick='confirmDelete(" . $row['id'] . ")'>Delete</button>";
    echo "<button style='background-color: #4BB543;' onclick='editUser(" . $row['id'] . ")'>Edit</button>";
    echo "<hr>";
}
echo "</div>";

 ?>   
	<div class="form-container">


		<form method="post" action="signup_process.php">
        <button type="button" ><a href="create_post.php">Create Post</a></button>
		</form>

	</div>
</body>
</html>
<script>
 const closeBtns = document.querySelectorAll('.close-btn');

closeBtns.forEach(btn => {
    btn.addEventListener('click', e => {
        e.target.parentElement.style.display = 'none';
    });
});

function confirmDelete(id) {
    if (confirm("Are you sure you want to delete this user?")) {
        window.location.href = "delete_post.php?id=" + id;
    }
}

function editUser(id) {
    if (confirm("Are you sure you want to edit this user?")) {
        window.location.href = "edit_post.php?id=" + id;
    }
}

    </script>