<?php

$host = 'localhost';
$username = 'root';
$password = '';
$database = 'rethink';
$conn = mysqli_connect($host, $username, $password, $database);

// Check if the connection to the database is successful
if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}