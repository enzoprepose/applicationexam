<?php
// Establish connection to the database
session_start(); 
include "db_conn.php";

// Get the user inputs from the form
$post_title = $_POST['post_title'];
$post_content = $_POST['post_content'];
$current_datetime = date("Y-m-d H:i:s");

// Check if the email address is already registered in the database
$sql = "SELECT * FROM post WHERE post_title='$post_title'";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
	header("Location: create_post.php?error=title_exists");
	exit();
}

// Add the user to the database
$sql = "INSERT INTO post (post_title, post_content, date_created) VALUES ('$post_title', '$post_title', '$current_datetime')";
if (mysqli_query($conn, $sql)) {
	header("Location: create_post.php?success=true");
	exit();
} else {
	echo "Error: " . mysqli_error($conn);
}

// Close the connection to the database
mysqli_close($conn);
?>
