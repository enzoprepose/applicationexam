<!DOCTYPE html>
<html>
<head>
	<title>Registration Form - MiniBlog</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<header>
		<h1>MiniBlog</h1>
		<a href="index.php">Login</a>
	</header>
    <h1 class="center-heading">Registration</h1>
	<div class="form-container">
		<h1 class="form-header">See the Registration Rules</h1>
		<?php
if (isset($_GET['error'])) {
    if ($_GET['error'] == 'error') {
        echo "<div class='error'><p>Error</p><span class='close-btn'>&times;</span></div>";
    }
} else if (isset($_GET['success'])) {
    if ($_GET['success'] == 'true') {
        echo "<div class='success'><p>User registered successfully</p><span class='close-btn'>&times;</span></div>";
    }
}
?>

		<form action="register.php" method="POST">
			<input type="text" placeholder="Enter Username" name="name" required>

			<input type="email" placeholder="Enter Email" name="email" required>

			<input type="password" placeholder="Enter Password" name="password" required>

			<input type="password" placeholder="Confirm Password" name="confirm_password" required>

			<input type="submit" name="submit" value="REGISTER">

            
		</form>
        <?php
        $text = "LOGIN PAGE";
        ?>
        <a href="index.php" class="login">Return to <?php echo "<a style='color: #ffab40;'>" . $text. "</a>" ?></a>

	</div>
</body>
</html>
<script>
 const closeBtns = document.querySelectorAll('.close-btn');

closeBtns.forEach(btn => {
    btn.addEventListener('click', e => {
        e.target.parentElement.style.display = 'none';
    });
});
    </script>