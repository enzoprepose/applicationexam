<!DOCTYPE html>
<html>
<head>
	<title>Registration Form - MiniBlog</title>
	<link rel="stylesheet" type="text/css" href="style1.css">
</head>
<body>
	<header>
		<h1>MiniBlog</h1>
		<a href="login.php">Login</a>
	</header>
	<div class="form-container">
		<h1 class="form-header">Login</h1>
		<?php
if (isset($_GET['error'])) {
if ($_GET['error'] == 'invalid_credentials') {
        echo "<div class='error'><p>Incorrect Credentials</p><span class='close-btn'>&times;</span></div>";
    }
}
?>

		<form method="post" action="signup_process.php">

			<input type="email" placeholder="Enter Email" name="email" required>

			<input type="password" placeholder="Enter Password" name="password" required>

			<input type="submit" name="submit" value="LOGIN">
            <a class="register" href="registration.php" name="submit" value="REGISTER">REGISTER</a>
		</form>

	</div>
</body>
</html>
<script>
 const closeBtns = document.querySelectorAll('.close-btn');

closeBtns.forEach(btn => {
    btn.addEventListener('click', e => {
        e.target.parentElement.style.display = 'none';
    });
});
    </script>