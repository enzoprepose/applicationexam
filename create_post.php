<?php
session_start();

if (isset($_SESSION['id']) && isset($_SESSION['username'])) {
}else{
    header("Location: index.php");
    exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Registration Form - MiniBlog</title>
	<link rel="stylesheet" type="text/css" href="style3.css">
</head>
<body>
	<header>
		<h1>MiniBlog</h1>
		<h1>Hi, <?php echo $_SESSION['username']; ?>!</h1>
        <a href="home.php">Home</a>
		<a href="logout.php">Log out</a>
	</header>
	<div class="form-container">
		<h1 class="form-header">Create a Post!</h1>
		<?php
if (isset($_GET['error'])) {
    if ($_GET['error'] == 'password_mismatch') {
        echo "<div class='error'><p>Passwords do not match</p><span class='close-btn'>&times;</span></div>";
    } else if ($_GET['error'] == 'title_exists') {
        echo "<div class='error'><p>Title already exists</p><span class='close-btn'>&times;</span></div>";
    }
} else if (isset($_GET['success'])) {
    if ($_GET['success'] == 'true') {
        echo "<div class='success'><p>Post Created</p><span class='close-btn'>&times;</span></div>";
    }
}
?>

		<form action="upload.php" method="POST">
			<input type="text" placeholder="Enter Title" name="post_title" required>

			<input type="text" placeholder="Enter Content" name="post_content" required>

			<input type="submit" name="submit" value="POST">

            
		</form>

	</div>
</body>
</html>
<script>
 const closeBtns = document.querySelectorAll('.close-btn');

closeBtns.forEach(btn => {
    btn.addEventListener('click', e => {
        e.target.parentElement.style.display = 'none';
    });
});
    </script>