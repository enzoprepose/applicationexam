<?php
// Establish connection to the database
session_start(); 
include "db_conn.php";

// Get the user inputs from the form
$name = $_POST['name'];
$email = $_POST['email'];
$password = $_POST['password'];
$confirm_password = $_POST['confirm_password'];

// Check if the password and confirm password match
if ($password !== $confirm_password) {
	header("Location: registration.php?error=password_mismatch");
	exit();
}

// Check if the email address is already registered in the database
$sql = "SELECT * FROM users WHERE email='$email'";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
	header("Location: registration.php?error=email_exists");
	exit();
}

// Hash the password for security
$hashed_password = password_hash($password, PASSWORD_DEFAULT);
// $hashed_password = $password;

// Add the user to the database
$sql = "INSERT INTO users (username, email, password) VALUES ('$name', '$email', '$hashed_password')";
if (mysqli_query($conn, $sql)) {
	header("Location: registration.php?success=true");
	exit();
} else {
	echo "Error: " . mysqli_error($conn);
}

// Close the connection to the database
mysqli_close($conn);
?>
